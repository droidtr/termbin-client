Termbin Android CLient
----------------------

Termbin is an open source pastebin service that used socket programming.
This application uses socket programming to simulate netcat.

The pendant to this app is available for Linux and can be found at [Github](https://github.com/solusipse/fiche).

Also see https://termbin.com/ for additional details.

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/org.droidtr.termbin/)