package org.droidtr.termbin;

import android.app.*;
import android.os.*;
import android.view.*;
import android.view.View.*;
import android.widget.*;
import java.io.*;
import java.net.*;
import android.graphics.*;

public class MainActivity extends Activity 
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy); 
		if (Build.VERSION.SDK_INT > 21) {
			setTheme(android.R.style.Theme_Material_Light_NoActionBar);
		} else if (Build.VERSION.SDK_INT > 14) {
			setTheme(android.R.style.Theme_Holo_Light_NoActionBar);
		} else {
			setTheme(android.R.style.Theme_Light_NoTitleBar);
		}
		LinearLayout.LayoutParams match = new LinearLayout.LayoutParams(-1,-1);
		match.weight=1f;
		super.onCreate(savedInstanceState);
		LinearLayout main =new LinearLayout(this);
        setContentView(main);
		final EditText t=new EditText(this);
		final EditText e=new EditText(this);
		e.setLayoutParams(match);
		e.setGravity(Gravity.TOP);
		LinearLayout ll1=new LinearLayout(this);
		ll1.setLayoutParams(match);
		LinearLayout ll2=new LinearLayout(this);
		ll1.addView(e);
		ll2.setLayoutParams(new LinearLayout.LayoutParams(-1,-2));
		Button b=new Button(this);
		ll2.addView(b);
		b.setLayoutParams(new LinearLayout.LayoutParams(-2,-1));
		t.setLayoutParams(match);
		ll2.addView(t);
		main.addView(ll2);
		main.addView(ll1);
		main.setOrientation(LinearLayout.VERTICAL);
		b.setText("Send");
		t.setText("");
		getWindow().setStatusBarColor(Color.parseColor("#546e7a"));
		ll2.setPadding(15,15,15,15);
		e.setTextColor(Color.parseColor("#263238"));
		t.setTextColor(Color.parseColor("#eceff1"));
		ll2.setBackgroundColor(Color.parseColor("#455a64"));
		b.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View p1)
				{
					new Handler().post(new Runnable(){

							@Override
							public void run()
							{
								t.setText(socketsend(e.getText().toString()).trim());
							}
						});
					
				}
			});
	}
	public void execNoWait(final String command){
		new AsyncTask<String,String,Void>(){
			@Override
			public Void doInBackground(String... params){
				execForStringOutput(command);
				return null;
			}
		}.execute();
	}
	public String execForStringOutput(String command) {
		try{
			String line;
			StringBuilder s = new StringBuilder();
			java.lang.Process p = Runtime.getRuntime().exec("sh");
			DataOutputStream dos = new DataOutputStream(p.getOutputStream());
			String[] clist=command.split(";");
			for(int i=0;i<clist.length;i++){
				dos.writeBytes(clist[i]+"\n");
				dos.flush();
			}
			dos.close();
			BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((line = input.readLine()) != null){
				if(line.trim().length()>0){
					s.append((line).trim()+"\n");
				}
			}
			input.close();
			return s.toString();
		}catch(Exception e){
			return "";
		}
	}
	public String socketsend(String data){
		String ret="";
		try{
		Socket socket = new Socket("termbin.com", 9999);

		DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());

		DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());

		dataOutputStream.writeBytes(data+"\n");
		dataOutputStream.flush();
		ret=dataInputStream.readLine();
		dataInputStream.close();
		dataOutputStream.close();
		socket.close();
		}catch(Exception e){
			ret=e.toString();
		}
		return ret;
	}
}
